import pycosat
import pickle

def find ( constraints ):
    print "Running solver with {} constraints...".format(
        len( constraints ))
    sat_solution = pycosat.solve( constraints )
    if sat_solution == 'UNKNOWN':
        raise RuntimeError
    if sat_solution == 'UNSAT':
        return False
    return sat_solution


readfile = open( "sat_c5k3", "r" )
sat_constraints = pickle.load( readfile )

if find( sat_constraints ) is not False:
    print 'YES!'
