#
# DO POLYMOPRHISMS FROM RAINBOW TO NAE SATISFY SOME MALTSEV CONDITIONS
#
#

import pycosat
import sys
import pickle

import random

def number2tup ( num, mod, dim ): # converts a~number to base n expansion
    curnum = num
    tup = []
    for digit in range( dim ): 
        tup.append( curnum % mod )
        curnum /= mod
    return tup

def tup2number ( tup, mod ): # converts a tuple in base mod into a number
    curnum = 0
    revtup = tup[::-1]
    for digit in revtup: 
        curnum *= mod
        curnum += digit
    return curnum

def value ( pol, tup ):
    for a in pol:
        if a[:-1] == tup:
            return a[-1]
    raise IndexError

#
# GENERATION OF CONSTRAINTS
#

def check( tup, q ):
    for a in range( q ):
        if not (a in tup):
            return False
    return True

def relation( q ):
    k = q + 1;
    rel = []
    for number in range( q**k ):
        tup = number2tup( number, q, k )
        if check( tup, q ):
            rel.append( tup )
    return rel

def polymorphism_constraints ( q, d ):
    # Constraints for being a d-ary polymoprhism form R_q to NAE_2
    k = q + 1


    # Variables:
    # 1 boolean variable for each vertex of R_q^d
    vertices = range( q**d )
    colors = range( q )

    # Constraints:
    # k-ary NAE in CNF
    # [ v_1, ..., v_k ], [ -v_1, ..., -v_k ]
    constraints = []
    rel = relation( q )
    rel_size = len( rel )
    
    for number in range(rel_size**d):
        tup = number2tup( number, rel_size, d )
        pre_constraint = [ rel[tup[i]] for i in range( d ) ]
        constraint = []
        for v in range( k ):
            constraint.append ( tup2number( [ pre_constraint[i][v] for i in
                range( d ) ], q ) + 1 )
        constraints.append( constraint )
        constraints.append( [ -v for v in constraint ] )
    return constraints

def minor_equality_constraints ( q, projection1, projection2 ):
    # projections are given as tuple of variables, i.e., [ 0, 1, 1 ] for
    # f(x,y,y)

    if (len( projection1 ) != len( projection2 )) or (
        len( set( projection1 ) ) != len( set( projection2 ))):
        raise IndexError
    
    d = len( projection1 )
    dmin = len( set( projection1 ) )

    def major( tup, projection ):
        majtup = [ tup[ i ] for i in projection ]
        maj = 0
        for digit in range(d-1, -1, -1):
            maj *= q
            maj += majtup[ digit ]
        return maj + 1

    def equals( u, v ):
        return [ [u,-v], [-u,v] ]

    constraints = []
    for a in range( q ** dmin ): # tuple of arity md
        tup = number2tup( a, q, dmin )
        constraints += equals( major( tup, projection1 ),
            major( tup, projection2 ) )

    return constraints

#
# THE SOLVER HOOKS
#

def find ( constraints ):
    #print "Running solver with {} constraints...".format(
        #len( constraints ))
    sat_solution = pycosat.solve( constraints )
    if sat_solution == 'UNKNOWN':
        raise RuntimeError
    if sat_solution == 'UNSAT':
        return False
    return sat_solution

def findone ( constraints, n, m, d ):

    def vertex( variable ):
        return (variable - 1) / m

    def color( variable ):
        return (variable - 1) % m

    def decode ( sat_output ):
        if sat_output is False:
            return False
        polymorphism = []
        for x in sat_output:
            if x > 0:
                value = number2tup( vertex( x ), n, d )
                value.append( color ( x ) )
                polymorphism.append( value )
        return polymorphism

    return decode( find( constraints ) )


def decode ( sat_output, q, d ):
    if sat_output is False:
        return False
    polymorphism = []
    for x in sat_output:
        if x > 0:
            value = number2tup( x-1, q, d )
            value.append( 1 )
        else:
            value = number2tup( -x-1, q, d )
            value.append( 0 )
        polymorphism.append( value )
    return polymorphism
#
# MAIN PART
#

q = 4

##
# CHECKING FOR OLSAK TERM
#
#constraints = polymorphism_constraints( q, 6 )
#constraints += minor_equality_constraints( q, [ 1, 0, 0, 0, 1, 1 ],
#    [0, 1, 0, 1, 0, 1] )
#constraints += minor_equality_constraints( q, [ 1, 0, 0, 0, 1, 1 ],
#    [0, 0, 1, 1, 1, 0] )

#print 'Instance constructed, now checking...'
#if find( constraints ) is False:
#   print 'Pol(C_{},K_{}) does not contain an Olsak.'.format( d, e )
#else:
#    print 'PCSP(C_{},K_{}) contains an Olsak.'.format( d, e )


##
# CHECKING FOR MAJORITY POLYMORPHISM
#
constraints = polymorphism_constraints( q, 3 )
constraints += minor_equality_constraints( q, [1, 0, 0], [0, 1, 0] )
constraints += minor_equality_constraints( q, [1, 0, 0], [0, 0, 1] )

print 'Instance constructed, now checking...'
sol =  find( constraints )
pol = decode( sol, q, 3 )
if pol is False:
    print 'Pol(R_{},NAE_2) does not contain a majority.'.format( q )
else:
    print 'PCSP(R_{},NAE_2) contains a majority.'.format( q )
    print sol
    for a in pol:
        print a

