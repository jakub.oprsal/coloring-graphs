##
# We want to check whether there is a recolouring sequence between two
# k-colourings of a given graph
#

import sys
import pickle
from copy import copy


# type graph:
#   [nvertices,list of edges]
    
def cycle(n):
    edges = [ [i,(i+1)%n] for i in range(n) ]
    return [n,edges]

def recolour(graph,map1,map2,length,colours):
    n,edges = graph

    filler = [ -1 for i in range(n) ]
    sequence = [ map1 ]
    for i in range(1,n):
        sequence.append( filler );
    sequence.append( map2 )

    

recolour(cycle(5),[0,1,2,0,1],[0,2,1,0,2],4,3)
