#
# DO POLYMOPRHISMS FROM 5-CYCLE TO SOME K_n HAVE A SIGGERS 
#
#

import pycosat
import sys
import pickle

import random

def number2tup ( num, mod, dim ): # converts a~number to base n expansion
    curnum = num
    tup = []
    for digit in range( dim ): 
        tup.append( curnum % mod )
        curnum /= mod
    return tup

def tup2number ( tup, mod ): # converts a tuple in base mod into a number
    curnum = 0
    revtup = tup[::-1]
    for digit in revtup: 
        curnum *= mod
        curnum += digit
    return curnum

def value ( pol, tup ):
    for a in pol:
        if a[:-1] == tup:
            return a[-1]
    raise IndexError

def print_bin_pol( pol, n ):
    for a, b, value in pol:
        sys.stdout.write( '{}'.format( value ) )
        if a == n-1:
            sys.stdout.write( "\n" )
            if b == n-1:
                print ""

#
# GENERATION OF CONSTRAINTS
#

def polymorphism_constraints ( n, m, d ):
    # Constraints for being a d-ary polymoprhism form C_n to K_m

    def edge ( x, y ):
        # Returns true iff each digit in base n of x and y differ by 1 modulo n
        curx = x
        cury = y
        for digit in range( d ):
            dx = curx % n
            dy = cury % n
            if (( dx != ( dy + 1 ) % n ) and ( dy != (dx + 1) % n )):
                return False
            curx /= n
            cury /= n
        return True

    def vertex( variable ):
        return variable / m

    def color( variable ):
        return variable % m

    # Variables:
    # d boolean variables for each vertex of K_n^d, each expressing `the
    # vertex {} has color {}'

    vertices = range( n**d )
    colors = range( m )
    variables = range( len(colors) * len( vertices ) )

    # Constraints:
    constraints = []

    # Each vertex has exatly one color (at least one and not 2 or more)
    for x in vertices:
        constraints.append( range( x*m + 1, (x+1)*m + 1 ) )
        for a in colors:
            for b in range( a+1, m ):
                constraints.append( [ -(x*m + a + 1), -(x*m + b + 1) ] )

    def neighbours( vertex ):
        v = number2tup( vertex, n, d )
        change = [ -1 for i in range( d ) ]

        def increment():
            # switch first -1, 1, \dots, 1 in change to 1, -1, \dots, -1
            i = 0
            while change[ i ] == 1:
                change[ i ] = -1
                i += 1
                if i >= d:
                    return False
            change[ i ] = 1
            return True

        stop = False
        nb = []
        while increment():
            nb.append ( tup2number(
                [ (sum( pair ) % n) for pair in zip( v, change ) ], n ) )
        return nb

    # No two vertices that share a coordinate, share a color
    for x in vertices:
        for y in neighbours( x ):
            for c in colors:
                constraints.append( [ -(x*m + c + 1), -(y*m + c + 1) ] )

    return constraints


def minor_constraints ( n, m, projection, minor ):
    # projection is given as tuple of variables, i.e., [ 0, 1, 1 ] for
    # f(x,y,y)
    # minor is given as a polymorphism
    
    d = len( projection )
    dmin = len( set( projection ) )

    def major( tup ):
        majtup = [ tup[ i ] for i in projection ]
        maj = 0
        for digit in range(d-1, -1, -1):
            maj *= n
            maj += majtup[ digit ]
        return maj

    constraints = []
    for a in range( n ** dmin ): # tuple of arity md
        tup = number2tup( a, n, dmin )
        constraints.append( [ major( tup )*m + value( minor, tup ) + 1 ] )

    return constraints

def minor_equality_constraints ( n, m, projection1, projection2 ):
    # projections are given as tuple of variables, i.e., [ 0, 1, 1 ] for
    # f(x,y,y)

    #if (len( projection1 ) != len( projection2 )) or (
        #len( set( projection1 ) ) != len( set( projection2 ))):
        #raise IndexError
    
    d = len( projection1 )
    dmin = len( set( projection1 ) )

    def major( tup, projection ):
        majtup = [ tup[ i ] for i in projection ]
        maj = 0
        for digit in range(d-1, -1, -1):
            maj *= n
            maj += majtup[ digit ]
        return maj

    def equals( u, v ):
        constraints = []
        for i in range(1, m+1):
            constraints.append( [ (u*m + i), -(v*m + i) ] )
            constraints.append( [ -(u*m + i), (v*m + i) ] )
        return constraints


    constraints = []
    for a in range( n ** dmin ): # tuple of arity md
        tup = number2tup( a, n, dmin )
        constraints += equals( major( tup, projection1 ),
            major( tup, projection2 ) )

    return constraints

#
# THE SOLVER HOOKS
#

def find ( constraints ):
    sat_solution = pycosat.solve( constraints )
    if sat_solution == 'UNKNOWN':
        raise RuntimeError
    if sat_solution == 'UNSAT':
        return False
    return sat_solution

def findone ( constraints, n, m, d ):

    def vertex( variable ):
        return (variable - 1) / m

    def color( variable ):
        return (variable - 1) % m

    def decode ( sat_output ):
        if sat_output is False:
            return False
        polymorphism = []
        for x in sat_output:
            if x > 0:
                value = number2tup( vertex( x ), n, d )
                value.append( color ( x ) )
                polymorphism.append( value )
        return polymorphism

    return decode( find( constraints ) )

def findall ( n, m, d ):
    # Find all d-ary polymorphisms from C_n to K_m

    def vertex( variable ):
        return (variable - 1) / m

    def color( variable ):
        return (variable - 1) % m

    def decode ( sat_output ):
        polymorphism = []
        for x in sat_output:
            if x > 0:
                value = number2tup( vertex( x ), n, d )
                value.append( color ( x ) )
                polymorphism.append( value )
        return polymorphism

    constraints = polymorphism_constraints( n, m, d )
    #constraints += minor_constraints( n, m, [ 0 for i in range( d ) ],
        #[ [a,a] for a in range( n ) ] )
        #[ [ 0, 0 ], [ 1, 1 ], [ 2, 2 ], [ 3, 0 ], [ 4, 0 ],  ] )

    polymorphisms = []
    i = 0
    count = 0
    for sat_solution in pycosat.itersolve( constraints ):
        polymorphisms.append( decode( sat_solution ) )
        sys.stdout.write('.')
        i += 1
        count += 1
        if i == 73:
            print( count )
            i = 0
    return polymorphisms

#
# MAIN PART
#

d = 7 # C_7
e = 4 # K_4

print 'Starting'

##
# CHECKING FOR SIGGERS TERM
#
#constraints = polymorphism_constraints( d, e, 6 )
#constraints += minor_equality_constraints( d, e, [ 0, 1, 2, 0, 1, 2 ],
#    [1, 2, 0, 2, 0, 1] )
#
#print 'Instance constructed, now checking...'
#if find( constraints ) is False:
#    print 'Pol(C_{},K_{}) does not contain a~Siggers.'.format( d, e )
#else:
#    print 'PCSP(C_{},K_{}) contains a~Siggers.'.format( d, e )

##
# CHECKING FOR OLSAK TERM
#
#constraints = polymorphism_constraints( d, e, 6 )
#constraints += minor_equality_constraints( d, e, [ 1, 0, 0, 0, 1, 1 ],
#    [0, 1, 0, 1, 0, 1] )
#constraints += minor_equality_constraints( d, e, [ 1, 0, 0, 0, 1, 1 ],
#    [0, 0, 1, 1, 1, 0] )
#
#print 'Instance constructed, now checking...'
#if find( constraints ) is False:
#    print 'Pol(C_{},K_{}) does not contain an Olsak.'.format( d, e )
#else:
#    print 'PCSP(C_{},K_{}) contains an Olsak.'.format( d, e )

##
# CHECKING FOR 4-ARY SIGGERS
#
#constraints = polymorphism_constraints( d, e, 4 )
#constraints += minor_equality_constraints( d, e, [ 0, 0, 1, 2 ],
#    [ 1, 2, 0, 1 ] )
#
#print 'Instance constructed, now checking...'
#pol = findone( constraints, d, e, 4 )
#if pol is False:
#    print 'Pol(C_{},K_{}) does not contain a 4-ary Siggers.'.format( d, e )
#else:
#    print 'PCSP(C_{},K_{}) contains a 4-ary Siggers.'.format( d, e )

##
# CHECKING FOR MAJORITY
#
constraints = polymorphism_constraints( d, e, 3 )
constraints += minor_equality_constraints( d, e, [ 0, 1, 0 ],
    [0, 0, 0] )
constraints += minor_equality_constraints( d, e, [ 0, 0, 1 ],
    [0, 0, 0] )
constraints += minor_equality_constraints( d, e, [ 1, 0, 0 ],
    [0, 0, 0] )

print 'Instance constructed, now checking...'
pol = findone( constraints, d, e, 3 )
if pol is False:
    print 'Pol(C_{},K_{}) does not contain a quasi-majority.'.format( d, e )
else:
    print 'Pol(C_{},K_{}) contains a quasi-majority.'.format( d, e )

##
# HOMOMORPHISM TO (1-in-3,NAE)
#

#print 'Looking for binary polymorphisms...'
#pols = findall( d, e, 2 )
#npols = len( pols )
#ipols = range( 1, npols + 1 )
#print 'There are {} binary polymoprhisms.'.format( npols )

#for pol in pols:
#    print_bin_pol( pol )

#base_constraints = polymorphism_constraints( d, e, 3 )
#def check_constraint( a,b,c ):
#    constraints = base_constraints[:]
#    constraints += minor_constraints( d, e, [ 0, 0, 1 ], pols[a - 1] )
#    constraints += minor_constraints( d, e, [ 0, 1, 0 ], pols[b - 1] )
#    constraints += minor_constraints( d, e, [ 1, 0, 0 ], pols[c - 1] )
#
#    if find( constraints ) is False:
#       return False
#    return True
#
#print 'Building a large instance of 1-in-3-Sat...'
#
#def oneinthree( x, y, z ):
#    return [ [ x, y, z ], [ -x, -y ], [ -x, -z ], [ -y, -z ] ]
#
#sat_constraints = []
#for a in ipols:
#    print 'a =', a
#    for b in range( a, npols + 1 ):
#        for c in range( b, npols + 1 ):
#            if check_constraint( a, b, c ):
#                sat_constraints += oneinthree( a, b, c )
#                sys.stdout.write('.')
#    print len( sat_constraints )
#
#dumpfile = open( "sat_cnk3", "w" )
#pickle.dump( sat_constraints, dumpfile )
#dumpfile.close()
#
#print 'Contraints dumped into "sat_cnk3"!'
