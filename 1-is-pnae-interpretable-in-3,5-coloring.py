#
# IS 2,4-NAE INTERPRETABLE IN 3,5 COLORING?
#
# The goal is to check whether there is a reduction between 2,4-NAE and
# graph 3,5-coloring proving that 3,5 coloring is NP-hard.
# We do that using colorings of free actions of minions. Namely, we check
# whether the free action of Pol(3,5) on 2-NAE has a~homomorphism into
# 4-NAE. That is done in few steps:
# 
# 1/ We find elements in the free action, i.e., all binary polymoprhisms
#   from K_3 to K_5.
#   This is to find all solutions to a certain CSP(K_5) instance.
#
# 2/ We find tuples in the free action, i.e., for each triple of elements
#   (binary polymoprhisms), we check whether they have a~certain common
#   major given by 2-NAE relation.
#   This are many instances of CSP(K_5 + const).
#
# 3/ We check a~homorphism from the constructed structure into 4-NAE.
#   This is a single instance of CSP(4-NAE).
#
# All CSPs are solved using a reduction to SAT, and consequently running
# pycosat solver.
#

import pycosat
import sys
import pickle

import random

def number2tup ( num, mod, dim ): # converts a~number to base n expansion
    curnum = num
    tup = []
    for digit in range( dim ): 
        tup.append( curnum % mod )
        curnum /= mod
    return tup

def value ( pol, tup ):
    for a in pol:
        if a[:-1] == tup:
            return a[-1]
    raise IndexError

#
# GENERATION OF CONSTRAINTS
#

def polymorphism_constraints ( n, m, d ):

    def edge ( x, y ):
        # Returns true iff x and y do NOT share a digit in base n
        curx = x
        cury = y
        for digit in range( d ):
            if ( curx % n ) == ( cury % n ):
                return False
            curx /= n
            cury /= n
        return True

    def vertex( variable ):
        return variable / m

    def color( variable ):
        return variable % m

    # Variables:
    # d boolean variables for each vertex of K_n^d, each expressing `the
    # vertex {} has color {}'

    vertices = range( n**d )
    colors = range( m )
    variables = range( len(colors) * len( vertices ) )

    # Constraints:
    constraints = []

    # Each vertex has exatly one color (at least one and not 2 or more)
    for x in vertices:
        constraints.append( range( x*m + 1, (x+1)*m + 1 ) )
        for a in colors:
            for b in range( a+1, m ):
                constraints.append( [ -(x*m + a + 1), -(x*m + b + 1) ] )

    # No two vertices that share a coordinate, share a color
    for x in vertices:
        for y in range( x+1, n**d ):
            if edge( x, y ):
                for c in colors:
                    constraints.append( [ -(x*m + c + 1), -(y*m + c + 1) ] )

    return constraints


def minor_constraints ( n, m, projection, minor ):
    # projection is given as tuple of variables, i.e., [ 0, 1, 1 ] for
    # f(x,y,y)
    # minor is given as a polymorphism
    
    d = len( projection )
    dmin = len( set( projection ) )

    def major( tup ):
        majtup = [ tup[ i ] for i in projection ]
        maj = 0
        for digit in range(d-1, -1, -1):
            maj *= n
            maj += majtup[ digit ]
        return maj

    constraints = []
    for a in range( n ** dmin ): # tuple of arity md
        tup = number2tup( a, n, dmin )
        constraints.append( [ major( tup )*m + value( minor, tup ) + 1 ] )

    return constraints

def minor_equality_constraints ( n, m, projection1, projection2 ):
    # projections are given as tuple of variables, i.e., [ 0, 1, 1 ] for
    # f(x,y,y)

    if (len( projection1 ) != len( projection2 )) or (
        len( set( projection1 ) ) != len( set( projection2 ))):
        raise IndexError
    
    d = len( projection1 )
    dmin = len( set( projection1 ) )

    def major( tup, projection ):
        majtup = [ tup[ i ] for i in projection ]
        maj = 0
        for digit in range(d-1, -1, -1):
            maj *= n
            maj += majtup[ digit ]
        return maj

    def equals( u, v ):
        constraints = []
        for i in range(1, m+1):
            constraints.append( [ (u*m + i), -(v*m + i) ] )
            constraints.append( [ -(u*m + i), (v*m + i) ] )
        return constraints


    constraints = []
    for a in range( n ** dmin ): # tuple of arity md
        tup = number2tup( a, n, dmin )
        constraints += equals( major( tup, projection1 ),
            major( tup, projection2 ) )

    return constraints

#
# THE SOLVER HOOKS
#

def find ( constraints ):
    #print "Running solver with {} constraints...".format(
        #len( constraints ))
    sat_solution = pycosat.solve( constraints )
    if sat_solution == 'UNKNOWN':
        raise RuntimeError
    if sat_solution == 'UNSAT':
        return False
    return sat_solution

def findall ( n, m, d ):
    #print "Looking for all {}-ary polymorphisms from K_{} to K_{}".format(
        #d, n, m )

    def vertex( variable ):
        return (variable - 1) / m

    def color( variable ):
        return (variable - 1) % m

    def decode ( sat_output ):
        polymorphism = []
        for x in sat_output:
            if x > 0:
                value = number2tup( vertex( x ), n, d )
                value.append( color ( x ) )
                polymorphism.append( value )
        return polymorphism

    constraints = polymorphism_constraints( n, m, d )
    constraints += minor_constraints( n, m, [ 0 for i in range( d ) ],
        [ [a,a] for a in range( n ) ] )

    #print "Running solver with {} constraints...".format(
        #len( constraints ))

    polymorphisms = []
    i = 0
    count = 0
    for sat_solution in pycosat.itersolve( constraints ):
        polymorphisms.append( decode( sat_solution ) )
        sys.stdout.write('.')
        i += 1
        count += 1
        if i == 50:
            #print( count )
            i = 0
    return polymorphisms

#
# BOOLEAN FUNCTIONS
#

def all_boolfunc( dim ):
    funcs = []
    for enc in range( 2**(2**dim-2) ):
        mid_enc = number2tup( enc, 2, 2**dim-2 )

        func = [  number2tup ( 0, 2, dim ) + [ 0 ] ]
        i = 1
        for val in mid_enc:
            func.append (  number2tup ( i, 2, dim ) + [ val ] )
            i += 1
        func.append (  number2tup ( i, 2, dim ) + [ 1 ] )
        funcs.append( func )
    return funcs

#
# MAPPING ON BINARIES
#
def xi( binary_pol ):
    for x in [0,1]:
        for y in [ [0,1],[1,2],[0,2] ]:
            if value ( binary_pol, [x,y[0]] ) == value (
                binary_pol, [x,y[1]] ):
                for x1 in range( x+1, 2 ):
                    if value ( binary_pol, [x1,y[0]] ) == value (
                        binary_pol, [x1,y[1]] ):
                        return 0
    return 1

def is_essential( func, variable ):
    for xuv in func:
        x = xuv[:-1]
        v = xuv[-1]
        y = x
        y[variable] = 1-x[variable]
        if not v == value( func, y ):
            return True
    return False

def all_essential( func ):
    arity = len( func[0] ) - 1
    for i in range( arity ):
        if not is_essential( func, i ):
            return False
    return True

#
# MAIN PART
#

#pols = findall( 3,5,2 )

#dumpfile = open( "pols_35_2", "w" )
#pickle.dump( pols, dumpfile )
#dumpfile.close()
 
#readfile = open ( "pols_35_2", "r" )
#pols = pickle.load( readfile )
#print len( pols )

#base_constraints = polymorphism_constraints( 3,5,6 )
#def check_constraint( a,b,c ):
    #constraints = base_constraints[:]
    #constraints += minor_constraints( 3, 5, [ 0, 0, 1, 1, 1, 0 ], pols[a] )
    #constraints += minor_constraints( 3, 5, [ 0, 1, 0, 1, 0, 1 ], pols[b] )
    #constraints += minor_constraints( 3, 5, [ 1, 0, 0, 0, 1, 1 ], pols[c] )

    #if find( constraints ) is False:
        #return False
    #return True

#edges = 0
#for tries in range( 1000 ):
    #a = random.randint( 0, len( pols ) -1 )
    #b = random.randint( a, len( pols ) -1 )
    #c = random.randint( b, len( pols ) -1 )
    #if check_constraint( a,b,c ):
        #edges += 1
#print edges


#def check_constraint( a,b,c ):
    #constraints = base_constraints[:]
    #constraints += minor_constraints( 3, 5, [ 0, 0, 1, 1, 1, 0 ], pols[a] )
    #constraints += minor_constraints( 3, 5, [ 0, 1, 0, 1, 0, 1 ], pols[b] )
    #constraints += minor_constraints( 3, 5, [ 1, 0, 0, 0, 1, 1 ], pols[c] )

    #if find( constraints ) is False:
        #return False
    #return True

#def check( num ):

    #i = 0
    #add = []

    #base_constraints = polymorphism_constraints( 3,5,6 )

    #for a in range( len( pols ) ):
        #a_constraints = base_constraints + minor_constraints( 3, 5, [ 0, 0, 1, 1, 1, 0 ], pols[a] )
        #for b in range( a, len( pols ) ):
            #b_constraints = a_constraints + minor_constraints( 3, 5, [ 0, 1, 0, 1, 0, 1 ], pols[b] )
            #for c in range( b, len( pols ) ):
                #c_constraints = b_constraints + minor_constraints( 3, 5, [ 1, 0, 0, 0, 1, 1 ], pols[c] )
                #if find( c_constraints ) is not False:
                    #add += [[a,b,c]]
                #i += 1
                #if i > num:
                    #return add

#def check( a ):
    #i = 0
    #add = []

    #base_constraints = polymorphism_constraints( 3,5,6 )
    #base_constraints += minor_constraints( 3, 5, [ 0, 0, 1, 1, 1, 0 ], pols[a] )
    #for b in range( a, len( pols ) ):
        #b_constraints = base_constraints + minor_constraints( 3, 5, [ 0, 1, 0, 1, 0, 1 ], pols[b] )
        #for c in range( b, len( pols ) ):
            #c_constraints = b_constraints + minor_constraints( 3, 5, [ 1, 0, 0, 0, 1, 1 ], pols[c] )
            #if find( c_constraints ) is not False:
                #add += [[a,b,c]]
            #i += 1
    #print i
    #return add

#print len( check( 380 ) )

#
# Simplified part
#

d = 3
e = 5
#pols = findall( d,e,2 )
#print len( pols )

constraints = polymorphism_constraints( d, e, 6 )
constraints += minor_equality_constraints( d, e, [ 0, 0, 1, 1, 1, 0 ],
    [0, 1, 0, 1, 0, 1] )
constraints += minor_equality_constraints( d, e, [ 1, 0, 0, 0, 1, 1 ],
    [0, 1, 0, 1, 0, 1] )

print 'Instance constructed, now checking...'
if find( constraints ) is False:
    print 'PCSP(K_{},K_{}) is NP-hard.'.format( d, e )
else:
    print 'There is no reduction from (NAE,alot-NAE).'

